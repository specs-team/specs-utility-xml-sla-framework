# README #
This repository contains the SPECS XML SLA Framework, which provides an XML representation of the SPECS Security SLAs.

The XML SLA Framework consists of several XML schemas, contained in the *schemas* folder: the main schema (/SLAtemplate/**SLATemplate.xsd**) is based on the WS-Agreement specification and provides a machine readable format for the representation of an SLA Template according to the SPECS Security SLA definition.
This main file includes other sub-schemas, which model specific security aspects (e.g., security metrics, security controls, security service level objectives) and are used to introduce security-related terms and guarantees in the SLA.

The following concepts have been modeled:

* control frameworks: supported security control frameworks. Currently, two control frameworks are supported in SPECS, namely the *NIST Security Control Framework* and the *CSA Cloud Control Matrix*. Related schemas for the representation of security controls are included in *nist* and *ccm* sub-folders (/control_frameworks/nist/**nist.xsd** and /control_frameworks/ccm/**ccm.xsd** files);

* security metrics: measurable parameters related to security characteristics of delivered services. The related schema (**security_metric.xsd**) is reported in the sub-folder *metrics*;

* security SLOs: conditions set on top of security metrics, representing the guarantees to fulfill. The related schema (**slo.xsd**) is included in the sub-folder *SLAtemplate*;

* security capabilities: security features to enforce on top of a service. The related schema (**capability.xsd**) is included in the sub-folder *SLAtemplate*;

* service description terms: service delivery terms including the type of service being delivered, the provider, the appliances and the associated security capabilities. The related  schema (**sdt.xsd**) is included in the sub-folder *SLAtemplate*.

The *examples* folder contains an example of SLA Template related to a Secure Web Container service, both in the XML and Word formats.